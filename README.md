# pihole-facebook
Facebook DNS Blocklist for Pihole.

## Adding this blacklist to pi-hole

Using the web interface:
1. Login into `Pi-hole admin`
2. Navigate to `Settings`
3. Expand `Pi-Hole's Block Lists`
4. Copy this URL: `https://gitlab.science.ru.nl/bram/pihole-facebook/raw/master/pihole-facebook.txt`
5. Paste the URL in the Edit box and click on `Save and update`

Or using the command line:
1. Add the following line to `/etc/pihole/adlists.list`
```
https://gitlab.science.ru.nl/bram/pihole-facebook/raw/master/pihole-facebook.txt
```
2. Update the list of ad-serving domains:
```
$ pihole updateGravity
```

You're done. 
